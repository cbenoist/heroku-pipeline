var request = require('supertest');
var app = require('../app.js')

describe('GET /', function() {
  it('displays "Salut World!"', function(done) {
    // The line below is the core test of our app.
    request(app)
      .get('/')
      .expect(function(res) {
        if (res.text.indexOf('Salut World!') == -1) throw new Error ("Missing Salut World!");
      })
      .expect(200, done);
  });
});
